export default ({ app }, inject) => {
  const removeFirstKey = (obj) => {
    const length = Object.keys(obj).length;
    const sliced = Object.keys(obj)
      .slice(1, length)
      .reduce((result, key) => {
        result[key] = obj[key];
        return result;
      }, {});
    return sliced;
  };
  const formatDate = (date) => {
    const monthNames = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ];
    let temp = date.split("-");
    let string =
      temp[2] + " " + monthNames[parseInt(temp[1]) - 1] + " " + temp[0];
    return string;
  };

  inject("removeFirstKey", removeFirstKey);
  inject("formatDate", formatDate);
};
